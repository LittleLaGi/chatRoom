var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var index = require('./routes/index');
var login = require('./routes/login');
var logout = require('./routes/logout');
 
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser('sjlsnsdf'));

app.use('/',index);
app.use('/login',login);
app.use('/logout',logout);

app.get('/t',(req, res)=>{
  res.sendFile(path.join(__dirname+'/routes/test.html'));
});

module.exports = app;

