var app = require('./app');
var http = require('http');
var mosca = require('mosca');

http.createServer(app).listen(process.env.PORT || 8080);

var pubsubsettings = {
  port: 1883
};

var server = new mosca.Server(pubsubsettings);
server.on('ready', setup);

function setup(){

}

server.on('publish',(packet, client)=>{
  try{
    var json_source = JSON.parse(packet.payload.toString('UTF-8'));
		var action = json_source.action;
    var target = json_source.target;
    var message = json_source.message;

    if(action === 'chat'){
      
    }else if(action === 'addFriend'){

    }else if(action === 'replyAddFriend'){

    }

    //console.log(packet.payload);
  }catch(error){
    console.log(error);
  }
});




server.on('clientConnected', function (client) {
	console.log('Client Connected:', client.id);
});

server.on('clientDisconnected', function (client) {
	console.log('Client Disconnected:', client.id);
});
