var express = require('express');
var path = require('path');
var clients = require('../accounts').clients;
var router = express.Router();

router.get('/', function (req, res) {
    var client = clients.find(a => a.account ===  req.signedCookies.user);
    if(!client){res.sendFile(path.join(__dirname+'/login.html'));}
    else res.send('welcome!');
})

module.exports = router;