var express = require('express');
var path = require('path');
var clients = require('../accounts').clients;
var router = express.Router();

router.post('/', function (req, res) {
    var client = clients.find(a => a.account === req.body.a && a.password === req.body.p);
    if(!client){res.sendFile(path.join(__dirname+'/login.html'));}
    else {
        res.cookie('user',client.account,{signed: true});
        res.send('welcome!');
    }
})

module.exports = router;