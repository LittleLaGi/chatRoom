var express = require('express');
var path = require('path');
var clients = require('../accounts').clients;
var router = express.Router();

router.get('/', function (req, res) {
    res.clearCookie('user',req.signedCookies.user,{signed: true});
    res.send('log out');
})

module.exports = router;